# rocky_vm

Rocky Linux VM basic configuration. 
Goal is to ease the basic configuration steps of a Rocky Linux 8.5 using Ansible (and some more basic stuff like storing aliases somewhere)

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
The plan is to enhance this configuration so that it becomes easy to create a new VM...

## Contributing
This is a somewhat private repo, as I use it for my own purposes. 
Suggestions are welcome if you came to know about it, of coure!

## Authors and acknowledgment

## License
For open source projects, say how it is licensed.

## Project status
